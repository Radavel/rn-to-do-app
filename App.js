import React from 'react';
import {
  StyleSheet, View, AsyncStorage,
} from 'react-native';
import Header from './Header';
import Body from './Body';

export default class App extends React.Component {
  constructor() {
    super();

    this.state = {
      tasks: [],
      inputText: '',
      loading: true,
    };
  }

  componentDidMount() {
    this.getTasksFromPhone();
  }

  addTask = () => {
    const newTasks = [...this.state.tasks, { task: this.state.inputText, key: Date.now() }];
    this.storeTasksInPhone(newTasks);
    this.setState({
      tasks: newTasks,
      inputText: '',
    });
  }

  updateInputText = (value) => {
    this.setState({
      inputText: value,
    });
  }

  deleteTask = (id) => {
    console.log(id);
    const newTasks = this.state.tasks.filter(task => task.key !== id);
    this.storeTasksInPhone(newTasks);
    this.setState({
      tasks: newTasks,
    });
  }

  storeTasksInPhone = (newTasks) => {
    AsyncStorage.setItem('@ToDoApp:tasks', JSON.stringify(newTasks))
      .then((value) => {
        console.log(value);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  getTasksFromPhone = () => {
    AsyncStorage.getItem('@ToDoApp:tasks')
      .then((value) => {
        console.log(value);
        console.log(JSON.parse(value));
        setTimeout(() => {
          this.setState({
            loading: false,
          });
        }, 5000);
        if (value !== null) {
          const savedTasks = JSON.parse(value);
          this.setState({
            tasks: savedTasks,
          });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          loading: false,
        });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          updateInputText={this.updateInputText}
          addTask={this.addTask}
          inputText={this.state.inputText}
        />
        <Body tasks={this.state.tasks} deleteTask={this.deleteTask} loading={this.state.loading} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
