// import liraries
import React, { Component } from 'react';
import {
  View, FlatList, StyleSheet, ActivityIndicator,
} from 'react-native';
import Task from './Task';

// create a component
class Body extends Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.loading
          && (
          <ActivityIndicator
            size="large"
            color="#646464"
          />
          )
        }
        {!this.props.loading
          && (
          <FlatList
            data={this.props.tasks}
            renderItem={({ item }) => <Task item={item} deleteTask={this.props.deleteTask} />}
          />
          )
        }
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 10,
    backgroundColor: '#fff',
  },
});

// make this component available to the app
export default Body;
