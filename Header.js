// import liraries
import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TextInput,
} from 'react-native';

// create a component
class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput style={styles.input} placeholder="Tarea" onSubmitEditing={this.props.addTask} value={this.props.inputText} onChangeText={this.props.updateInputText} />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2c3e50',
    paddingTop: 50,
    paddingLeft: 15,
    paddingRight: 15,
  },
  title: {
    color: '#fff',
    fontSize: 20,
  },
  input: {
    fontSize: 24,
    color: '#fff',
    padding: 5,
  },
});

// make this component available to the app
export default Header;
