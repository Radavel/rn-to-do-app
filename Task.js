import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

class Task extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.item.task}</Text>
        <TouchableOpacity onPress={() => { this.props.deleteTask(this.props.item.key); }}>
          <Ionicons
            name="ios-close-circle-outline"
            size={24}
            color="gray"
          />
        </TouchableOpacity>
      </View>
    );
  }
}
export default Task;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
    paddingHorizontal: 16,
  },
  text: {
    fontSize: 24,
  },
});
